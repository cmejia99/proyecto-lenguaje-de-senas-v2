from tensorflow.keras.preprocessing import image_dataset_from_directory
import tensorflow as tf
import numpy as np
import time
from sklearn.cluster import KMeans
from sklearn.preprocessing import StandardScaler
import cv2

from keras.preprocessing.image import ImageDataGenerator, array_to_img, img_to_array, load_img
import glob
import numpy as np
import imageio

###Clase que tiene el proposito de cargar los datos del data set.
class dataSet:
    def __init__(self,width,height,batch_size):
        self.dir_base='..\\DataSet\\'        
        self.width = width
        self.height = height
        self.batch_size = batch_size
        self.train_ts = None
        self.test_ts = None
        self.train_ts, self.test_ts = self.Load_dataSet()        
    def Load_dataSet(self):
        train_dt = image_dataset_from_directory(directory=self.dir_base, ##Directorio de la imagenes.
                                                labels='inferred', ##Etiquetas apartir del directorio.
                                                label_mode='categorical', ##Modo categorico.
                                                batch_size=self.batch_size, ##Tamaño de lotes.
                                                image_size=(self.width, self.height), ##Escala a este tamaño las imagenes.
                                                seed = 1,
                                                validation_split = 0.2, ##Porcentaje del tamaño del data set.
                                                #validation_split = 0.8, ##Porcentaje del tamaño del data set.
                                                subset = "training" ##Definición del tipo de data set que es, en este caso validación.
                                                )        
        test_dt = image_dataset_from_directory(directory=self.dir_base, ##Directorio de la imagenes.
                                                labels='inferred', ##Etiquetas apartir del directorio.
                                                label_mode='categorical', ##Modo categorico.
                                                batch_size=self.batch_size, ##Tamaño de lotes.
                                                image_size=(self.width, self.height), ##Escala a este tamaño las imagenes.
                                                seed = 1,
                                                validation_split = 0.2, ##Porcentaje del tamaño del data set.
                                                #validation_split = 0.1, ##Porcentaje del tamaño del data set.
                                                subset = "validation" ##Definición del tipo de data set que es, en este caso validación.
                                                )     
        return train_dt, test_dt
    def Normalization(self,data_set):
        nor_ly = tf.keras.layers.experimental.preprocessing.Rescaling(1./255) 
        new_ds = data_set.map(lambda x, y: (nor_ly(x), y)) 
        ###Visualizamos los pixeles maximos y minimos del data set:
        image_batch1, labels_batch1 = next(iter(data_set))
        first_image1 = image_batch1[0]
        image_batch, labels_batch = next(iter(new_ds))
        first_image = image_batch[0]
        #Se saca el valor del pixel maximo y minimo:
        print('Imagen Original', np.min(first_image1), np.max(first_image1))
        print('Imagen Escalada',np.min(first_image), np.max(first_image))
        return new_ds
    ###Funcion que devuelve los datos en objeto lista
    def generated_vector(self,ds,x_ds,y_ds):     
        iterador = iter(ds)  
        while True:
            try:          
                batch = next(iterador)
                iterador2 = iter(batch)    
                images = next(iterador2)
                labels = next(iterador2)        
                it_img = iter(images)    
                it_lab = iter(labels)    
                while True:    
                    try:
                        image = next(it_img)    
                        label = next(it_lab)
                        x_ds.append(image.numpy())
                        y_ds.append(label.numpy())
                    except StopIteration:
                        break                        
            except StopIteration:
                break          
        return x_ds,y_ds  
    ##Función que retorna las entradas en tipo numpy    
    def list_to_numpy(self,data,label):
        x_data= np.array(data)
        y_label= np.array(label)    
        return x_data,y_label 
    ###Funcion que devuelve los datos en objeto lista mejorado
    def generated_vector_nw(self,ds,x_ds,y_ds):
        i = 0
        iterador = iter(ds)  
        batch = next(iterador)
        x_ds = batch[0].numpy()
        y_ds = batch[1].numpy()
        while True:
            try:          
                i += 1                
                batch = next(iterador)
                x_ds = np.concatenate((x_ds, batch[0].numpy()), axis=0)
                y_ds = np.concatenate((y_ds, batch[1].numpy()), axis=0)                
            except StopIteration:
                break          
        return x_ds,y_ds
    ##Funcion para cargar el data set dado a comet.
    def load_image_comet(self,experiment,x_ds,y_ds,labels,name):
        itera = 0
        for i in x_ds:            
            letra = np.argmax(y_ds[itera])            
            experiment.log_image(i, name=name+labels[letra], overwrite=False,image_shape=(200,200,3))            
            itera+=1
    ##   
    def preprocesando(self,img):  
        sift = cv2.SIFT.create()
        img_g= cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)    
        img_g=img_g.astype('uint8')
        sift = cv2.xfeatures2d.SIFT_create()
        kp,desc = sift.detectAndCompute(img_g,None)        
        return kp,desc  
    def generar_dataset(self,x_ds,y_ds,Features,Labels): 
        i = 0
        for img in x_ds:      
          kp, desc = self.preprocesando(img)      
          Features.append(desc)
          Labels.append(y_ds[i])      
          i+=1
        return Features,Labels 
    def apilamiento_descriptores(self,Features):
        vStack = np.array(Features[0])        
        for values in Features[1:]:        
            vStack = np.vstack((vStack, values))           
        return vStack   
    def procesamiento_datos (self,caracteristicas,etiquetas):
        #Conviritendo las listas a arreglos
        np_features=np.array(caracteristicas)
        np_labels= np.array(etiquetas)
        ##Se redimensiona el arreglo de etiquetas:
        #np_labels = np_labels.reshape(len(np_labels),1)
        #concatenamos cada vector de caracteristicas con las etiquetas:
        np_dataset=np.hstack((np_features,np_labels))
        #np_features = np_dataset[:,:-1]
        #np_labels = np_dataset[:,-1].reshape(len(np_labels),1)    
        #np_labels = np_dataset[:,-1]
        return np_features,np_labels,np_dataset
    def genera_histograma(self,clusters,Total_imagenes,features,kmeans_model):
        ###Se genera el histograma vacio del total de cluster por cada imagen
        inicio = time.time()
        histogram_global = np.array([np.zeros(clusters) for i in range(Total_imagenes)])
        print(histogram_global.shape)
        #Se construye el histograma de agrupamiento por imagen de los grupos del cluster:
        idx_imagen = 0
        for i in range(Total_imagenes):  ###Total de imagenes:   
            num_fea_img = len(features[i])    
            for j in range(num_fea_img): ##Se corre por los descriptores de la imagen:
                idx_hist = kmeans_model[idx_imagen+j] ##Se consulta el grupo del cluster
                histogram_global[i][idx_hist] += 1 ##Se cuenta el grupo al que pertenece,
            idx_imagen += num_fea_img  ##Cambiamos de imagen
        fin = time.time()
        print('##########################################################')
        print('Total tiempo generacion histograma', (fin-inicio))
        print('##########################################################')
        return histogram_global
    def data_augmentation(self,letter):
        datagen = ImageDataGenerator(rotation_range=20, width_shift_range=0.1, zca_epsilon=0.1, brightness_range=[0.1,0.8],
                            zoom_range=[0.8, 0.8]) #Hyperparaemtros del objeto generador
        filenames = glob.glob('../DataSet/'+letter+'/*') #Leyendo solo el subfolder A
        for f in filenames:
          img = load_img(f)
          i2a = img_to_array(img)
          i2a = i2a.reshape((1, )+ i2a.shape)
          #print(i2a.shape)

          i=1

          #Generando 1 imagen por cada imagen original
          for batch in datagen.flow(i2a, batch_size=1,
                                    save_to_dir ='../DataSet/'+letter,
                                    save_prefix='aug',
                                    save_format ='jpg'):
            i=i+1
            if i > 2: #aqui puede cambiar el número de imágenes que usted desee generar por una original
              break
          #break 