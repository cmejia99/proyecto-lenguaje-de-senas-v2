
from tensorflow import keras
import tensorflow as tf
import tensorflow.keras.layers as ly
import tensorflow.keras.models as md
import time
from matplotlib import pyplot as plt


class models:
    def __init__(self,width,height,epochs):
        self.width = width
        self.height = height
        self.epochs = epochs
    def cnn_v1(self):
        inputs = ly.Input(shape=(self.width,self.height,3)) 
        conv1 = ly.Conv2D(32,(3,3),padding='same', activation='relu')(inputs) 
        conv2 = ly.Conv2D(32,(3,3),padding='same', activation='relu')(conv1) 
        pool1 = ly.MaxPool2D((2,2),(2,2))(conv2)
        conv3 = ly.Conv2D(64,(3,3),padding='same', activation='relu')(pool1) 
        conv4 = ly.Conv2D(64,(3,3),padding='same', activation='relu')(conv3) 
        pool2 = ly.MaxPool2D((2,2),(2,2))(conv4) 
        conv5 = ly.Conv2D(128,(3,3),padding='same', activation='relu')(pool2) 
        conv6 = ly.Conv2D(128,(3,3),padding='same', activation='relu')(conv5) 
        pool3 = ly.MaxPool2D((2,2),(2,2))(conv6) 
        flat =  ly.Flatten()(pool3) 
        FC1 = ly.Dense(128,activation='relu')(flat)
        FC2 = ly.Dense(64,activation='relu')(FC1)
        outputs = ly.Dense(24,activation='softmax')(FC2) 
        return md.Model(inputs,outputs)
    def cnn_v2(self):
        inputs = ly.Input(shape=(self.width,self.height,3)) 
        conv1 = ly.Conv2D(32,(3,3),padding='same', activation='relu')(inputs) 
        conv2 = ly.Conv2D(32,(3,3),padding='same', activation='relu')(conv1) 
        pool1 = ly.MaxPool2D((2,2),(2,2))(conv2)
        drop1 = ly.Dropout(0.4)(pool1)
        conv3 = ly.Conv2D(64,(3,3),padding='same', activation='relu')(drop1) 
        conv4 = ly.Conv2D(64,(3,3),padding='same', activation='relu')(conv3) 
        pool2 = ly.MaxPool2D((2,2),(2,2))(conv4) 
        drop2 = ly.Dropout(0.4)(pool2)
        conv5 = ly.Conv2D(128,(3,3),padding='same', activation='relu')(drop2) 
        conv6 = ly.Conv2D(128,(3,3),padding='same', activation='relu')(conv5) 
        pool3 = ly.MaxPool2D((2,2),(2,2))(conv6) 
        drop3 = ly.Dropout(0.4)(pool3)
        flat =  ly.Flatten()(drop3) 
        FC1 = ly.Dense(128,activation='relu')(flat)
        drop4 = ly.Dropout(0.4)(FC1)
        FC2 = ly.Dense(64,activation='relu')(drop4)
        drop5 = ly.Dropout(0.4)(FC2)
        outputs = ly.Dense(24,activation='softmax')(drop5) 
        return md.Model(inputs,outputs)
    def cnn_v3(self):
        inputs = ly.Input(shape=(self.width,self.height,3)) 
        conv1 = ly.Conv2D(32,(3,3),padding='same', activation='relu')(inputs) 
        conv2 = ly.Conv2D(32,(3,3),padding='same', activation='relu')(conv1) 
        pool1 = ly.MaxPool2D((2,2),(2,2))(conv2)        
        conv3 = ly.Conv2D(64,(3,3),padding='same', activation='relu')(pool1) 
        conv4 = ly.Conv2D(64,(3,3),padding='same', activation='relu')(conv3) 
        pool2 = ly.MaxPool2D((2,2),(2,2))(conv4)         
        conv5 = ly.Conv2D(128,(3,3),padding='same', activation='relu')(pool2) 
        conv6 = ly.Conv2D(128,(3,3),padding='same', activation='relu')(conv5) 
        pool3 = ly.MaxPool2D((2,2),(2,2))(conv6)         
        flat =  ly.Flatten()(pool3) 
        FC1 = ly.Dense(128,activation='relu')(flat)
        drop1 = ly.Dropout(0.4)(FC1)
        FC2 = ly.Dense(64,activation='relu')(drop1)
        drop2 = ly.Dropout(0.4)(FC2)
        outputs = ly.Dense(24,activation='softmax')(drop2) 
        return md.Model(inputs,outputs)
    def Generated_model_nogpu(self,model,optimo):
        tf.keras.backend.clear_session
        model.compile(optimizer=optimo, loss='categorical_crossentropy',metrics=['accuracy'] )        
        return model
    def Generated_model(self,model,optimo):
        tf.keras.backend.clear_session
        strategy = tf.device('/device:GPU:0')        
        with strategy:           
          model.compile(optimizer=optimo, loss='categorical_crossentropy',metrics=['accuracy'] )        
        return model
    def execute_model(self,model,Data_train, Data_test):
        inicio = time.time()
        history = model.fit(Data_train, epochs=self.epochs,validation_data=Data_test,verbose=1)
        loss, acc = model.evaluate(Data_test, verbose=1)
        fin = time.time()
        print('Tiempo ejecución modelo: ', round((fin-inicio),2))
        return history, loss, acc            
    def graphics (self,history,text):
        plt.figure(figsize=(15,10))
        plt.subplot(2,2,1)
        plt.plot(history.history['accuracy'])
        plt.plot(history.history['val_accuracy'])
        plt.title('Modelo de accuracy '+text)
        plt.ylabel('Accuracy')
        plt.xlabel('Epochs')
        plt.legend(['train','test'], loc='upper left')
        plt.subplot(2,2,2)
        plt.plot(history.history['loss'])
        plt.plot(history.history['val_loss'])
        plt.title('Modelo de Loss '+text)
        plt.ylabel('Loss')
        plt.xlabel('Epochs')
        plt.legend(['train','test'], loc='upper left')
        plt.show()