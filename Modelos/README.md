# **Proyecto lenguaje de señas EEUU Versión 1.0** #
## **Diseñado por:**

* *Carlos Arbey Mejia Martinez*
    * **Código:** 2210549
* *Andres Felipe Guerra* 
    * **Código:** 2211058

## **Contenido Modelos** ##
En este repositorio se podrá encontrar los fuentes y resultados generados en la construcción del modelo de predicción del problema del proyecto.

El repositorio se estructura de la siguiente manera:

* **Codigo:** Directorio donde se registró notebook **/CM/lengsenafinal2.ipynb** el cual contiene el fuente de la zona de captura o región de seña, el cual tiene como propósito encerrar en un recuadro la mano detectada, una vez realizado esta tarea se procede a enviar esta región en un formato de imagen al modelo de predicción construido.

    Otro propósito del archivo **lengsenafinal2.ipynb** es servir como constructor de imágenes para capturar nuevos fuentes para el dataset de entrenamiento. Las imágenes capturadas por el notebook se almacenaran en el directorio **img** en formado jpg.

    En el directorio **Modelo** se registro el fuente del modelo de predicción implementado para la clasificación de la seña realizada por el usuario, aprobado para nuestra solución productiva.

    Para el funcionamiento de **lengsenafinal2.ipynb** se debe tener instalada las siguientes librerías de Python:

    ```python
    pip install opencv-contrib-python==4.4.0.44
    pip install mediapipe
    pip install tensorflow
    ```
    También se tiene que tener ubicada en la misma carpeta el archivo **classData.py** el cual contiene la clase con los métodos utilizados para el funcionamiento del notebook.

* **Modelos:** Directorio donde se registró los notebook utilizados en la construcción del proyecto:
    
    1. **Validacion_modelos.ipynb:** Notebook con los experimentos realizados localmente, aquí se puede evidenciar la ejecución del modelo CNN y Sift propuestos en el proyecto, además su respectiva conexión con el repositorio de comet.ml donde se registró y almaceno los resultados del proyecto.
    2. **web.ipynb y web1.ipynb:** Notebook con los experimentos realizados en Google Colab dado que algunos experimentos sobrepasaban los recueros locales que se tenían en el proyecto.
    3. **classData .py:** Archivo Python con la clase construida para la manipulación de información y construcción del modelo Sift.
    4. **Modelos .py:** Archivo Python con la clase creada para almacenar las redes CNN propuesta para el proyecto.
    5. **Modelos:** Directorio donde se almaceno todos los archivos de los pesos de las modelos entrenados:

        Para el funcionamiento de los archivos fuentes del repositorio se requiere instalar las siguientes librerías de Python:
        ```python
        pip install opencv-contrib-python==4.4.0.44
        pip install tensorflow==2.5.0
        pip install comet_ml
        pip install matplotlib
        pip install -U scikit-learn
        CUDA==11.2
        cuDNN==8.1
        ```

        Para los modelos de entrenamiento se utilizaron los siguientes hiperparámetros:

        | optimizador | learning_rate | momentum | epochs | 
        |---|---|---|---|
        | Stochastic Gradient Descent | 0.001 | 0.9 | 20 |        

        * **Modelo_1.h5:** Modelo Inicial classData --> **cnn_v1** + Repositorio depurado.    
        * **Modelo_2.h5:** Modelo Inicial classData --> **cnn_v2** + Repositorio depurado
        * **Modelo_3.h5:** Modelo Inicial classData --> **cnn_v3** + Repositorio depurado
        * **Modelo_4.h5:** Modelo Inicial classData --> **cnn_v1** con data augmentation.
        * **Modelo_5.h5:** Modelo Inicial classData --> **cnn_v2** con data augmentation.
        * **Modelo_6.h5:** Modelo Inicial classData --> **cnn_v3** con data augmentation.
        * **Modelo_5_1.h5:** Modelo Inicial classData --> **cnn_v2** repositorio original (Sin depuración) + data augmentation.
        * **Kmeans.pkl y StandardScaler.pkl** Modelo Sift. Dado los resultados iniciales con este modelo propuesto, se decidió dar todo el esfuerzo y tiempo a los modelos CNN por lo que no se continuo con la experimentación de este propuesta.
    6. **Fuentes:** Directorio con fuentes revisados para la construcción de los modelos del proyecto.

* **Resultados:** Directorio donde se registró los archivos de imágenes con los resultados de los modelos iniciales, previo a la documentación de resultados en 
Comet.ml

## **Links alternos al repositorio:** ##
* [MediaPipe](https://mediapipe.dev/)
* [TensorFlow GPU](https://www.tensorflow.org/install/source#gpu)
* [Comet.ml](https://www.comet.ml/cmejia99/lenguaje-de-senas/view/new)
