#!/usr/bin/env python
# coding: utf-8

# In[ ]:


from flask import Flask, render_template, request, redirect, url_for, flash, make_response, abort
import cv2
import mediapipe as mp
import os
import numpy as np
from scipy.spatial import distance
from tensorflow import keras
import copy
import math
import requests

modelo_prd = keras.models.load_model('Modelo_prd.h5')
clases = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'K', 'L', 'M',
          'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y']
font = cv2.FONT_HERSHEY_SIMPLEX
#new_predictions = new_model.predict(test_feat)
#letra = np.argmax(new_predictions)
#print('Modelo 1: Letra indicada por el modelo: ',nombres_clases[letra],'Probabilidad',new_predictions[0][letra])

mp_drawing = mp.solutions.drawing_utils # Plantilla de Dibujo de Mediapipe
mp_hands = mp.solutions.hands
mp_drawing_styles = mp.solutions.drawing_styles

cap = cv2.VideoCapture(0, cv2.CAP_DSHOW) # Leer el video
i=0
#x=0.5  # start point/total width
#y=0.8  # start point/total width
threshold = 60  # BINARY threshold
blurValue = 7  # GaussianBlur parameter
bgSubThreshold = 50
learningRate = 0
app = Flask(__name__)

@app.route('/')
def return_object():
    
    def removeBG(frame): #Subtracting the background
        fgmask = bgModel.apply(frame,learningRate=learningRate)
        kernel = np.ones((3, 3), np.uint8)
        fgmask = cv2.erode(fgmask, kernel, iterations=1)
        res = cv2.bitwise_and(frame, frame, mask=fgmask)
        return res

    with mp_hands.Hands( # Se especifica que se van a tomar las dos manos
        static_image_mode=False,
        max_num_hands=1,
        min_detection_confidence=0.5,
        min_tracking_confidence=0.5) as hands:

        while True: # Se realiza la activación de detección de la camara
            ret, frame = cap.read()
            if ret == False:
                break

            posiciones = [] # Lista donde se van a almacenar las coordenadas de los puntos
            height, width, c = frame.shape # Tamaño del frame        
            frame = cv2.flip(frame, 1)   
            copia = frame.copy() # Se genera copia del frame antes de dibujar lo puntos        
            #cv2.imshow('Original',copia)        
            frame_rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            #frame.flags.writeable = False
            results = hands.process(frame_rgb) # Resultado de las manos    

            if results.multi_hand_landmarks: # Coordenadas de puntos de las manos
                for mano in results.multi_hand_landmarks:
                    for id, ln in enumerate(mano.landmark): # Información de la mano con el ID

                        corx, cory = int(ln.x*width), int(ln.y*height)
                        posiciones.append([id,corx,cory])
                        mp_drawing.draw_landmarks(
                        frame, mano, mp_hands.HAND_CONNECTIONS,
                        mp_drawing.DrawingSpec(color=(0,100,100), thickness=1, circle_radius=5),
                        mp_drawing.DrawingSpec(color=(100,0,100), thickness=1, circle_radius=5))

                        ############# DIFERENCIANDO DEDOS #################

                        #mp_drawing_styles.get_default_hand_landmarks_style(),
                        #mp_drawing_styles.get_default_hand_connections_style()) 

                    if len (posiciones) !=0:

                        centro = posiciones[9] # Punto central de la mano
                        #print('centro',centro)
                        longitud = 100
                        error = 0 # 15
                        x1, y1 = (centro[1] - longitud),(centro[2] + longitud+error) # Se Restan 100 en los dos ejes
                        ancho, alto = (x1+120), (y1+150) # Se suma 120 en ancho y 150 en alto
                        ##x2, y2 = x1 + ancho, y1 + alto 
                        x2, y2 = (centro[1] + longitud),(centro[2] - (longitud+10)) # Se Restan 100 en los dos ejes 

                        region = copia[y2:y1, x1:x2] 
                        #bgModel = cv2.createBackgroundSubtractorMOG2(0, bgSubThreshold)
                        #img = removeBG(frame)
                        #img = img[0:int(y * frame.shape[0]),
                         #           int(x * frame.shape[1]):frame.shape[1]]  # clip the ROI
                        #cv2.imshow('mask', img)
                        # convert the image into binary image
                        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
                        blur = cv2.GaussianBlur(gray, (blurValue, blurValue), 0)
                        #cv2.imshow('blur', blur)
                        ret, thresh = cv2.threshold(blur, threshold, 255, cv2.THRESH_BINARY) #thresholding the frame
                        #cv2.imshow('ori', thresh)

                        # get the coutours
                        thresh1 = copy.deepcopy(thresh)
                        contours, hierarchy = cv2.findContours(thresh1, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE) #detecting contours
                        length = len(contours)
                        maxArea = -1
                        if length > 0:
                            for i in range(length):  # find the biggest contour (according to area)
                                temp = contours[i]
                                area = cv2.contourArea(temp)
                                if area > maxArea:
                                    maxArea = area
                                    ci = i
                            if region is None:
                                print('Imagen vacia')
                            else:
                                #cv2.imwrite('img//letras'+str(i)+'.jpg',region) ###Guardar imagen region
                                region_resize = cv2.resize(region, dsize=(200, 200), interpolation=cv2.INTER_CUBIC)
                                region_norm = (region_resize-region_resize.min())/(region_resize.max()-region_resize.min())
                                sing_to_pred = region_norm[None,:,:]                                                        
                                print('original',np.min(region_resize), np.min(region_norm),'max',
                                             np.max(region_resize), np.max(region_norm))  

                                res = contours[ci]
                                hull = cv2.convexHull(res) #applying convex hull technique
                                drawing = np.zeros(region_norm.shape, np.uint8)
                                cv2.drawContours(drawing, [res], 0, (0, 255, 0), 2) #drawing contours 
                                cv2.drawContours(drawing, [hull], 0, (0, 0, 255), 3) #drawing convex hull

                                # Predicion segun modelo
                                predictions = modelo_prd.predict(sing_to_pred)
                                letra = np.argmax(predictions)
                                letra_str = clases[letra]          
                                probabilidad = round(predictions[0][letra],3)
                                print('Modelo 1: Letra indicada por el modelo: ',letra_str,'Probabilidad',predictions[0][letra])
                                cv2.putText(frame,'LETRA '+str(letra_str)+'  '+ str(probabilidad),
                                           (x2-200, y2), font, 1, (255, 0, 0), 2, cv2.LINE_AA)
                                #print(type(region))
                                cv2.rectangle(frame, (x1,y1), (x2,y2), (255, 0, 0),1) # Muestra el frame

                        #reg = cv2.resize(frame,(ancho,alto), interpolation = cv2.INTER_AREA) # Redimensionar la imagen
                        #cv2.imwrite('Letra'+str(i)+'.jpg',frame)
                        cv2.imshow('Region',region_norm) # Muestra la región de la mano 
                        #cv2.imshow('Contornos', drawing)
                        #i+=1
            
            cv2.imshow('Lenguaje de Sena',frame) # Muestra el frame
            if cv2.waitKey(1) == 27: # Oprimir ESC para parar video
                break
            
    cap.release()
    cv2.destroyAllWindows()
    return render_template("index2.html")
    
    # Posición de la mano identificada
    print('Letra:', results.multi_handedness)

    # Keypoints de la mano identificada
    #print('Hand landmarks:', results.multi_hand_landmarks)
    
if __name__ == "__main__":
    app.run()   


# In[ ]:




