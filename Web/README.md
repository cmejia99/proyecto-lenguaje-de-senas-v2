# **Proyecto lenguaje de señas EEUU Versión 1.0** #
## **Diseñado por:**

* *Carlos Arbey Mejia Martinez*
    * **Código:** 2210549
* *Andres Felipe Guerra* 
    * **Código:** 2211058

## **Contenido Web** ##
En este repositorio se podrá encontrar las fuentes del entorno web y el modelo aprobado para el ambiente productivo.

Aquí el sistema se divide en dos partes:

1.  Sitio web donde se podrá consultar la información del proyecto, este sitio se encuentra publicado en internet en el link  [LENGUAJE DE SEÑAS EEUU](https://gristlier-maintaina.000webhostapp.com/#dataset)

2. Sitio local donde se despliegue el modelo de captura y predicción de las señas que podrá ingresar el usuario.

Para que el entorno de predicción funcione correctamente se debe ejecutar el archivo **lengsena.py** localmente, el cual utiliza funciones flask para publicar la funcionalidad.

Como prerrequisito se debe tener instalado en el entorno donde se vaya a ejecutar el archivo **lengsena.py** las siguientes librerías:

```python
    pip install opencv-contrib-python==4.4.0.44
    pip install mediapipe
    pip install tensorflow==2.5.0        
    pip install jinja2
    CUDA==11.2
    cuDNN==8.1
```
Para la ejecución del archivo **lengsena.py** se ejecutara el comando:
```python
    python lengsena.py --ip 0.0.0.0 --port 8000
```
El sistema mostrará la siguiente información en la consola donde se ejecuto el programa:

```cmd
 * Serving Flask app 'lengsena' (lazy loading)
 * Environment: production
   WARNING: This is a development server. Do not use it in a production deployment.
   Use a production WSGI server instead.
 * Debug mode: on
INFO: Created TensorFlow Lite XNNPACK delegate for CPU.
 * Running on all addresses.
   WARNING: This is a development server. Do not use it in a production deployment.
 * Running on http://192.168.20.20:8000/ (Press CTRL+C to quit)
```
## **Links alternos al repositorio:** ##
* [MediaPipe](https://mediapipe.dev/)
* [TensorFlow GPU](https://www.tensorflow.org/install/source#gpu)
