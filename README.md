# **Proyecto lenguaje de señas EEUU Versión 1.0** #
## **Diseñado por:**

* *Carlos Arbey Mejia Martinez*
    * **Código:** 2210549
* *Andres Felipe Guerra* 
    * **Código:** 2211058

## **Contenido** ##
En este repositorio se podrá encontrar la documentación, fuentes y resultados llevados a cabo en la construcción del proyecto V1.0 de detección de letras del lenguaje de señas de los Estados Unidos de América.

El repositorio se estructura de la siguiente manera:

* **DataSet:** Directorio donde se registró todos los archivos de imágenes clasificados por letras utilizados para el entrenamiento del modelo de IA.
* **Documentacion:** Directorio donde se registró todos los documentos construidos en el proyecto.
* **Modelos:** Directorio donde se registró todos los fuentes, modelos y resultados generados en la construcción del proyecto.
* **Web:** Directorio donde se registró los fuentes del sitio web y el modelo IA aprobado para producción cuya tarea es la predicción de la letra realizada por el usuario.

## **Links alternos al repositorio:** ##
* [Repositorio pruebas Comet.ml](https://www.comet.ml/cmejia99/lenguaje-de-senas/view/new)
* [Tablero Jira ](https://arturo-duque.atlassian.net/jira/software/projects/CA/boards/2/roadmap?shared=&atlOrigin=eyJpIjoiZDc0YWYzYTNhNzcxNGM4Y2I0NjEyNjdkYzg0MTQ0OTkiLCJwIjoiaiJ9)
* [Sitio Web del proyecto](https://gristlier-maintaina.000webhostapp.com/)
