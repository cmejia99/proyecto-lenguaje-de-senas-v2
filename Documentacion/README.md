# **Proyecto lenguaje de señas EEUU Versión 1.0** #
## **Diseñado por:**

* *Carlos Arbey Mejia Martinez*
    * **Código:** 2210549
* *Andres Felipe Guerra* 
    * **Código:** 2211058

## **Contenido Documentación** ##
En este repositorio se podrá encontrar la documentación generada en la construcción del proyecto.

El repositorio se estructura de la siguiente manera:

* **Bitácora:** Directorio donde se podrán encontrar las actas de las reuniones de los Sprint, los cuales se hicieron de una vez por semana.
* **Casos de uso:** Directorio donde se registró los documentos de caso de uso, el diagrama de proceso propuesto para producción y diagrama UML.
* **Desarrollo de software:** Directorio donde se registró dos imágenes de apuntes de los criterios iniciales de evaluación del proyecto para la materia de desarrollo de software IA de la universidad Autónoma.
* **Presentaciones:** Directorio donde se registró la presentación inicial del problema como opción de proyecto para la materia de desarrollo de software.
* **Documentación proyecto.docx:** Documento donde se registró información inicial del proyecto, la versión de la información se podrá encontrar más actualizada en los siguientes repositorios:
    * [Repositorio pruebas Comet.ml](https://www.comet.ml/cmejia99/lenguaje-de-senas/view/new)
    * [Tablero Jira](https://arturo-duque.atlassian.net/jira/software/projects/CA/boards/2/roadmap?shared=&atlOrigin=eyJpIjoiZDc0YWYzYTNhNzcxNGM4Y2I0NjEyNjdkYzg0MTQ0OTkiLCJwIjoiaiJ9)
    * [Sitio Web del proyecto](https://gristlier-maintaina.000webhostapp.com/)
