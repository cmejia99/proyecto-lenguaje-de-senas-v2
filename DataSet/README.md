# **Proyecto lenguaje de señas EEUU Versión 1.0** #
## **Diseñado por:**

* *Carlos Arbey Mejia Martinez*
    * **Código:** 2210549
* *Andres Felipe Guerra* 
    * **Código:** 2211058

## **Contenido DataSet** ##
En este repositorio se podrá encontrar los archivos de imágenes utilizados para el entrenamiento de los modelos propuestos e implementados para la solución del problema del proyecto. Estos archivos se encuentran organizados en carpetas correspondientes a la letra a la que pertenece cada imagen.

Para las construcción del dataset se utilizaron fuentes de otros proyectos como ,. Adicionalmente para estos fuentes, se realizó los siguientes procesos:

*   Validación y depuración de los fuentes para eliminar imágenes que podrías generar problemas o ruido en el entrenamiento.
*   Se generaron nuevos fuentes.
*   Se realiza un data augmentation utilizando la librería de Keras.

**Importante:** Podrá notar que no existen carpetas para las letras **J** y **Z**, El motivo de esto se da que la interpretación de estas letras con lleva un movimiento, y el propósito inicial de este proyecto es realiza predicción sobre imágenes secuenciales independientes, por lo que la predicción de este tipo de letras se tiene pensado realizarlo en una nueva versión del proyecto.

Adicionalmente podrá encontrar dos archivos ejecutables de Windows:

* inicio.cmd
* total_muestras.ps1

El propósito de estos archivos es poder listar rápidamente en consola Windows el total de imágenes que se encuentran por carpetas.

La utilización de este archivo solo implicará dar clic sobre el archivo inici.cmd el cual generará una salida de la siguiente manera:

```cmd
A Total imagenes: 2736
B Total imagenes: 2941
C Total imagenes: 2945
D Total imagenes: 2675
E Total imagenes: 2878
F Total imagenes: 2973
G Total imagenes: 2845
H Total imagenes: 3060
I Total imagenes: 2952
K Total imagenes: 2998
L Total imagenes: 2960
M Total imagenes: 2725
N Total imagenes: 2865
O Total imagenes: 2979
P Total imagenes: 3164
Q Total imagenes: 2628
R Total imagenes: 3268
S Total imagenes: 3214
T Total imagenes: 2941
U Total imagenes: 2903
V Total imagenes: 3043
W Total imagenes: 2922
X Total imagenes: 3056
Y Total imagenes: 3054
``` 

## **Fuentes utilizados en el repositorio:** ##
* [ASL Alphabet](https://www.kaggle.com/grassknoted/asl-alphabet)
* [Indian-Sign-Language-Recognition](https://github.com/imRishabhGupta/Indian-Sign-Language-Recognition)

